﻿namespace Lands.viewModels
{
    using Lands.views;
    using System;
    using System.Collections.Generic;
    using System.Text;
    
    class MainViewModel
    {
        #region Viewmodels
        public LoginViewModel Login
        {
            get;
            set;

        }


        public  LandsViewModel Lands
        {
            get;
            set;
        }

        #endregion


        #region Constructors
        public MainViewModel()
        {
            this.Login = new LoginViewModel();
        }

        #endregion

        #region Singleyton

        private static MainViewModel instance;

        public static MainViewModel GetInstance()
        {
            if (instance == null)
            {
                return new MainViewModel();
            }

            return instance;
        }
        #endregion


    }
}
