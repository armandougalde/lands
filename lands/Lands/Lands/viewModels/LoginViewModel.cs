﻿

namespace Lands.views
{
    using GalaSoft.MvvmLight.Command;
    using Lands.viewModels;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Text;
    using System.Windows.Input;
    using Xamarin.Forms;

    class LoginViewModel : BaseViewModel
    {

        #region Attributes

       
        private string password;
        private bool isRunning;
        private bool isEnabled;
        private string email;
        
        #endregion

        #region Properties
        public string Email
        {
            get { return this.email; }
            set
            {
                SetValue(ref this.email, value);
            }
        }

        public bool IsEnabled
        {
            get
            {
                return isEnabled;
            }

            set { SetValue(ref this.isEnabled, value); }
        }

        public string Password {



            get
            {
                return password;
            }

            set { SetValue(ref password, value); }

        }


        public bool IsRunning
        {
            get
            {
                return this.isRunning;
            }

            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsRemembered { get; set; }

        #endregion

        #region Commands

        public ICommand LoginCommand

        {
            get
            {
                return new RelayCommand(Login);

            }



        }



        private async void Login()
        {
            if (string.IsNullOrEmpty(this.Email))
            {
                await Application.Current.MainPage.DisplayAlert(
                      "Error",
                      "You must enter an Email",
                      "Accept");
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                await Application.Current.MainPage.DisplayAlert(
                      "Error",
                      "You must enter an Password",
                      "Accept");
            }

            this.IsRunning = false;
            this.IsEnabled = true;

            if (this.Email != "armandougalde@gmail.com" || this.Password != "1234")
            {
                await Application.Current.MainPage.DisplayAlert(
                      "Error",
                      "Email o password wrong",
                      "Accept");
                this.Password = string.Empty;
                return;
            }

            this.IsRunning = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;

            MainViewModel.GetInstance().Lands = new LandsViewModel();

            await Application.Current.MainPage.Navigation.PushAsync(new LandsPage());
        }



        #endregion

        #region constructors

        public LoginViewModel()
        {
            this.IsRemembered = true;
            this.isEnabled = true;
        }
        #endregion





    }
}
