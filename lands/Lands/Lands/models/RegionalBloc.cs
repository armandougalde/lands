﻿namespace Lands.models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Text;
  public  class RegionalBloc
    {
        [JsonProperty(propertyName: "acronym")]
        public string Acronym { get; set; }
        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }
       

    }
}
