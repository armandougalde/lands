﻿namespace Lands.models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.Text;

    public class Currency
    {
        [JsonProperty(propertyName:"code")]
        public string Code { get; set; }

        [JsonProperty(propertyName: "name")]
        public string Name { get; set; }

        [JsonProperty(propertyName: "code")]
        public string Symbol { get; set; }

    }





}
